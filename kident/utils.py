from ipware import get_client_ip
from django.contrib.contenttypes.models import ContentType
from django_user_agents.utils import get_user_agent
from kommons.utils.model import get_model_from_str
from kommons.utils.http import get_request_site, get_session_key
from kommons.utils.generic import boolify

from django.contrib.auth.models import User
from kident.models import Account, Software, Hardware, Identity, Resource, ResourceRequest


class KidentUtils:

    def __init__(self, request):
        self.request = request

    def build_identity(self, app_info):
        user_agent = get_user_agent(self.request)

        dt = {
            'software': {
                'app_name': app_info['APP_NAME'],
                'app_id': app_info['APP_ID'],
                'version_name': app_info['VERSION_NAME'],
                'version_number': app_info['VERSION_NUMBER'],
                'platform': app_info['PLATFORM'],
                'medium': app_info['MEDIUM'],
              },
            'hardware': {
                'is_physical_device': not user_agent.is_bot,
                'device_id': self.request.COOKIES.get('device_id', ''),
                'device_name': user_agent.os.family,
                'release': user_agent.os.version_string,
                'model': user_agent.browser.version_string,
                'brand': user_agent.browser.family
            },
            'account': {
                'username': (self.request.user.username if self.request.user.is_authenticated else ''),
                'account_id': get_session_key(self.request),
            },
            'network': {
                'ip_address': get_client_ip(self.request)[0],
            }
        }

        return {'identity': dt}


class KidentSaveUtils:

    def __init__(self, request):
        self.request = request
        self.site = get_request_site(request)

    def save_request(self, data):
        resource, identity = None, None
        resource_dt = data.get('resource')
        identity_dt = data.get('identity')

        if resource_dt:
            resource = self.save_resource(resource_dt)

        if identity_dt:
            identity = self.save_identity(identity_dt)

        if resource and identity:
            access_type = resource_dt.get('access_type', 'load')
            ip_address = identity_dt.get('network', {}).get('ip_address', '')
            if not ip_address:
                ip_address = get_client_ip(self.request)[0]

            return ResourceRequest.objects.create(
                site=self.site,
                resource=resource,
                identity=identity,
                ip_address=ip_address,
                access_type=access_type,
                request_info=self.get_request_info(data),
                content_type=self._get_content_type(resource_dt),
                object_id=resource_dt.get('object_id', None)
            )

    def get_request_info(self, data):
        return {
            'method': self.request.method,
            'path': self.request.path,
            'query': dict(self.request.GET.items()),
            'post': dict(self.request.POST.items()),
            'body': data,
            'headers': self.request.headers
        }

    def save_resource(self, data):
        group = data.get('group')
        sub_group = data.get('sub_group')

        if group and sub_group:
            return Resource.objects.get_or_create(site=self.site, group=group, sub_group=sub_group)[0]

    def save_identity(self, data):
        account = self._create_account(data.get('account', {}))
        software = self._create_software(data.get('software', {}))
        hardware = self._create_hardware(data.get('hardware', {}))

        if account and software and hardware:
            return Identity.objects.get_or_create(account=account, software=software, hardware=hardware)[0]

    def _create_account(self, data):
        account_id = data.get('account_id', '')
        if account_id:
            return Account.objects.update_or_create(
                site=self.site,
                account_id=account_id,
                user=self._get_user(data.get('username', ''))
            )[0]

    def _create_software(self, data):
        medium = data.get('medium', '')
        app_name = data.get('app_name', '')
        app_id = data.get('app_id', '')

        if medium and app_name and app_id:
            return Software.objects.update_or_create(
                site=self.site,
                medium=medium,
                app_name=app_name,
                app_id=app_id,
                defaults={
                    'platform': data.get('platform', ''),
                    'version_name': data.get('version_name', ''),
                    'version_number': data.get('version_number', ''),
                }
            )[0]

    def _create_hardware(self, data):
        release = data.get('release', '')
        device_name = data.get('device_name', '')
        model = data.get('model', '')
        brand = data.get('brand', '')

        if device_name and release and model and brand:
            return Hardware.objects.update_or_create(
                site=self.site,
                device_name=device_name,
                release=release,
                model=model,
                brand=brand,
                defaults={
                    'is_physical_device': boolify(data.get('is_physical_device', '')),
                }
            )[0]

    def _get_user(self, username):
        if username:
            return User.objects.filter(username=username).first()

    def _get_content_type(self, data):
        app_model = data.get('app_model')
        if app_model:
            model_class = get_model_from_str(app_model)
            if model_class:
                return ContentType.objects.get_for_model(model_class)
