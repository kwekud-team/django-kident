# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from jsonfield import JSONField


class AbstractModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


# Add IP address to this model
class Account(AbstractModel):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.CASCADE)
    account_id = models.CharField(max_length=250)

    def __str__(self):
        if self.user:
            return self.user.email or self.user.username
        return self.account_id

    class Meta:
        unique_together = ('site', 'account_id', 'user')


class Software(AbstractModel):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    platform = models.CharField(max_length=250)
    app_name = models.CharField(max_length=250)
    medium = models.CharField(max_length=250)
    app_id = models.CharField(max_length=250, null=True, blank=True)
    version_name = models.CharField(max_length=250, null=True, blank=True)
    version_number = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.medium

    class Meta:
        unique_together = ('site', 'medium', 'app_name', 'app_id')


class Hardware(AbstractModel):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    is_physical_device = models.BooleanField(default=False)
    device_name = models.CharField(max_length=250)
    release = models.CharField(max_length=250)
    model = models.CharField(max_length=250)
    brand = models.CharField(max_length=250)
    device_id = models.CharField(max_length=250, null=True, blank=True)  # remove

    def __str__(self):
        return f'{self.device_name} - {self.release}'

    class Meta:
        unique_together = ('site', 'device_name', 'release', 'model', 'brand')


class Identity(AbstractModel):
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    software = models.ForeignKey(Software, on_delete=models.CASCADE)
    hardware = models.ForeignKey(Hardware, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.account} - {self.software} - {self.hardware}'

    class Meta:
        unique_together = ('account', 'software', 'hardware')


class Resource(AbstractModel):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    group = models.CharField(max_length=100)
    sub_group = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.group} - {self.sub_group}'

    class Meta:
        unique_together = ('site', 'group', 'sub_group')


ACCESS_TYPES = (
    ('load', 'Load'),
    ('click', 'Click'),
)


class ResourceRequest(AbstractModel):
    site = models.ForeignKey('sites.Site', on_delete=models.CASCADE)
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE)
    identity = models.ForeignKey(Identity, on_delete=models.CASCADE)
    ip_address = models.CharField(max_length=50, null=True, blank=True)
    access_type = models.CharField(max_length=20, choices=ACCESS_TYPES)
    request_info = JSONField(default=dict, blank=True)

    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, null=True, blank=True)
    content_object = GenericForeignKey()

    def __str__(self):
        return str(self.date_created)
