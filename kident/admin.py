from django.contrib import admin
#
from kident.models import Account, Software, Hardware, Identity, Resource, ResourceRequest
from kident.attrs import AccountAttr, SoftwareAttr, HardwareAttr, IdentityAttr, ResourceAttr, ResourceRequestAttr


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = AccountAttr.admin_list_display
    list_filter = AccountAttr.admin_list_filter
    search_fields = ('account_number',)


@admin.register(Software)
class SoftwareAdmin(admin.ModelAdmin):
    list_display = SoftwareAttr.admin_list_display
    list_filter = SoftwareAttr.admin_list_filter
    search_fields = ('package_id', 'platform', 'medium', 'app_name')


@admin.register(Hardware)
class HardwareAdmin(admin.ModelAdmin):
    list_display = HardwareAttr.admin_list_display
    list_filter = HardwareAttr.admin_list_filter
    search_fields = ('device_id', 'brand')


@admin.register(Identity)
class IdentityAdmin(admin.ModelAdmin):
    list_display = IdentityAttr.admin_list_display


@admin.register(Resource)
class ResourceAdmin(admin.ModelAdmin):
    list_display = ResourceAttr.admin_list_display
    list_filter = ResourceAttr.admin_list_filter
    search_fields = ('group', 'sub_group')


@admin.register(ResourceRequest)
class ResourceRequestAdmin(admin.ModelAdmin):
    list_display = ResourceRequestAttr.admin_list_display
    list_filter = ResourceRequestAttr.admin_list_filter
