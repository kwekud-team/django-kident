from django.db import models
from dataclasses import dataclass
from kommons.utils.model import get_str_from_model


@dataclass
class KidentResource:
    group: str
    sub_group: str
    access_type: str = 'load'
    content_object: models.Model = None

    def to_json(self):
        dt = {'group': self.group, 'sub_group': self.sub_group, 'access_type': self.access_type}
        if self.content_object:
            dt.update({
                'app_model': get_str_from_model(self.content_object),
                'object_id': self.content_object.id
            })

        return {'resource': dt}
