from rest_framework import viewsets

from kident.utils import KidentSaveUtils


class KidentViewSet(viewsets.ViewSet):
    resource_request = None

    def initial(self, request, *args, **kwargs):
        self.request.kident_resource = KidentSaveUtils(request).save_request(request.data)
        return super().initial(request, *args, **kwargs)

    # def finalize_response(self, request, response, *args, **kwargs):
    #     # self.resource_request = KidentSaveUtils(request).save_request(request.data)
    #     # print ("finalize_response", self.resource_request)
    #     return super().finalize_response(request, response, *args, **kwargs)
