# Generated by Django 3.0.7 on 2020-07-12 01:50

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import jsonfield.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('sites', '0002_alter_domain_unique'),
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('account_id', models.CharField(max_length=250)),
                ('site', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('site', 'account_id', 'user')},
            },
        ),
        migrations.CreateModel(
            name='Hardware',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('is_physical_device', models.BooleanField(default=False)),
                ('device_name', models.CharField(max_length=250)),
                ('release', models.CharField(max_length=250)),
                ('model', models.CharField(max_length=250)),
                ('brand', models.CharField(max_length=250)),
                ('device_id', models.CharField(blank=True, max_length=250, null=True)),
                ('site', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
            options={
                'unique_together': {('site', 'device_name', 'release', 'model', 'brand')},
            },
        ),
        migrations.CreateModel(
            name='Identity',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kident.Account')),
                ('hardware', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kident.Hardware')),
            ],
        ),
        migrations.CreateModel(
            name='Resource',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('group', models.CharField(max_length=100)),
                ('sub_group', models.CharField(max_length=100)),
                ('site', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
            options={
                'unique_together': {('site', 'group', 'sub_group')},
            },
        ),
        migrations.CreateModel(
            name='Software',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('platform', models.CharField(max_length=250)),
                ('app_name', models.CharField(max_length=250)),
                ('medium', models.CharField(max_length=250)),
                ('package_id', models.CharField(blank=True, max_length=250, null=True)),
                ('version_name', models.CharField(blank=True, max_length=250, null=True)),
                ('version_number', models.CharField(blank=True, max_length=250, null=True)),
                ('site', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
            options={
                'unique_together': {('site', 'medium', 'app_name', 'package_id')},
            },
        ),
        migrations.CreateModel(
            name='ResourceRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('ip_address', models.CharField(blank=True, max_length=50, null=True)),
                ('access_type', models.CharField(choices=[('load', 'Load'), ('click', 'Click')], max_length=20)),
                ('request_info', jsonfield.fields.JSONField(blank=True, default=dict)),
                ('object_id', models.PositiveIntegerField(blank=True, null=True)),
                ('content_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
                ('identity', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kident.Identity')),
                ('resource', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kident.Resource')),
                ('site', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='sites.Site')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='identity',
            name='software',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='kident.Software'),
        ),
        migrations.AlterUniqueTogether(
            name='identity',
            unique_together={('account', 'software', 'hardware')},
        ),
    ]
