from enum import Enum


class KidentK(Enum):
    QT_GROUP = 'qt-group'
    QT_QUESTION = 'question'
    QT_CHOICE = 'qt-choice'
    QT_SUB_QUESTION = 'qt-sub-question'
    QT_CUSTOM = 'qt-custom'
