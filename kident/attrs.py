
class AccountAttr:
    admin_list_display = ('account_id', 'user', 'site')
    admin_list_filter = ('site',)


class SoftwareAttr:
    admin_list_display = ('app_id', 'platform', 'medium', 'app_name', 'version_name', 'version_number', 'site')
    admin_list_filter = ('medium', 'platform', 'app_name')


class HardwareAttr:
    admin_list_display = ('site', 'is_physical_device', 'device_id', 'device_name', 'release', 'model', 'brand',)
    admin_list_filter = ('is_physical_device',)


class IdentityAttr:
    admin_list_display = ('account', 'software', 'hardware',)


class ResourceAttr:
    admin_list_display = ('group', 'sub_group', 'site')
    admin_list_filter = ('group',)


class ResourceRequestAttr:
    admin_list_display = ('date_created', 'resource', 'identity', 'ip_address', 'access_type', 'content_object')
    admin_list_filter = ('date_created', 'resource', 'access_type',)
